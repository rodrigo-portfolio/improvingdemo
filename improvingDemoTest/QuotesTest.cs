using improvingDemo.Controllers;
using improvingDemo.DB;
using improvingDemo.DB.Connection;
using improvingDemo.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace improvingDemoTest
{
    //It�s Important change the file within appsettings file to the ORIGINAL short verion of DB in order this tests works
    public class QuotesTest
    {
        private Mock<IQuotesDB> _quotesDB;
        QuotesDict _dict;
        QuotesController _quotesController;

        [SetUp]
        public void Setup()
        {
            _quotesDB = new Mock<IQuotesDB>();

            _quotesDB.Setup(x => x.GetById(1))
                .Returns(new Quote { Id = 1, Text = "123", Author = "asd" });

            _quotesDB.Setup(x => x.Delete(1))
                .Returns(new Quote());


            _dict = QuotesDict.Instance;
            _quotesController = new QuotesController(_quotesDB.Object);
        }

        [Test]
        public void CheckCombinationsShortFile()
        {
            int TotalNumber = 19;
            long combinations = _dict.getUniquePairs(TotalNumber);

            Assert.AreEqual(0, combinations);

            TotalNumber = 22;
            combinations = _dict.getUniquePairs(TotalNumber);

            Assert.AreEqual(1, combinations);

            TotalNumber = 32;
            combinations = _dict.getUniquePairs(TotalNumber);

            Assert.AreEqual(6, combinations);

            TotalNumber = 40;
            combinations = _dict.getUniquePairs(TotalNumber);

            Assert.AreEqual(7, combinations);

            TotalNumber = 200;
            combinations = _dict.getUniquePairs(TotalNumber);

            Assert.AreEqual(21, combinations);
        }


        [Test]
        public void CheckStatusGET()
        {
            var response = _quotesController.Get(new SearchParameters() { page = 0, itemsPerPage = 10 });

            Assert.IsInstanceOf<OkObjectResult>(response.Result);
        }

        [Test]
        public void CheckStatusGETBYID()
        {
            var response = _quotesController.Get( 0 );

            Assert.IsInstanceOf<Microsoft.AspNetCore.Mvc.NotFoundResult>(response.Result);

            response = _quotesController.Get(1);

            Assert.IsInstanceOf<OkObjectResult>(response.Result);
        }

        [Test]
        public void CheckStatusPOST()
        {
            var response = _quotesController.Post(new Quote() { Id = 0, Author = "asd", Text = "123" });

            Assert.IsInstanceOf<CreatedResult>(response.Result);
        }

        [Test]
        public void CheckStatusPUT()
        {
            var quote = new Quote() { Id = 1, Author = "asda", Text = "1234" };
            _quotesDB.Setup(x => x.Edit(quote))
                .Returns(new Quote());

            var response = _quotesController.Put(quote);

            Assert.IsInstanceOf<OkObjectResult>(response.Result);

            //Trying to edit unexisting quote

            response = _quotesController.Put(new Quote() { Id = 7, Author = "asda", Text = "1234" });

            Assert.IsInstanceOf<NotFoundResult>(response.Result);
        }

        [Test]
        public void CheckStatusDELETE()
        {
            var response = _quotesController.Delete(1);

            Assert.IsInstanceOf<OkObjectResult>(response.Result);

            //Trying to edit unexisting quote

            response = _quotesController.Delete(7);

            Assert.IsInstanceOf<NotFoundResult>(response.Result);
        }
    }
}