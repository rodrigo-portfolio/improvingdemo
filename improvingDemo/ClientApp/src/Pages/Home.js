import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>Hello, mates!</h1>
        <p>I really hope you like this</p>
        <p></p>
        <p>I would like to explain that I took part of the fron-end code from a previous project I developed to manage patient information for an aunt's clinic, I'm leaving the link to that repo if you want to take a look at it</p>
        <ul>
          <li><a href='https://gitlab.com/Rodrigo.epz/consultorios'>Rodrigo´s project Repo</a></li>
        </ul>
        
        <p>In order to use the <code>Quotes CRUD</code> please click on "Quotes" in the upper right hand corner</p>

        <p>The <code>ClientApp</code> subdirectory is a standard React application based on the <code>create-react-app</code> template. But I added more recent technology as hooks in the new components I Built.
        For the backend my approach was create a Dictionary with the relation of the author´s Ids and the length of the text, then when the user requests to calculate the combination of quotes, I only take the 
        items with a minor length of the length requested, then i order that values inr order to  just loop the half of the total list checking wich sum of values satisfied the condition. Anyways you can see the
        algorithm inside of QuotesDict class, the method calls "getUniquePairs".</p>

        <p>If you want to test the ShortDB file is necessary change the file name within <code>Appsettings.json</code> file </p>
      </div>
    );
  }
}
