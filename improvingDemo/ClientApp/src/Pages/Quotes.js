﻿import React, { Fragment, useEffect, useState } from "react";
import { useHistory } from 'react-router-dom'
import { Controller, set, useForm } from "react-hook-form";
import { Button, Col, Input, Label, Row, Form, Spinner } from "reactstrap";
import { QuotesApi } from "../Services/QuotesAPI";
import ModalCrud from "../Components/Quotes/ModalCRUD"
import CustomTable from "../Components/Quotes/Table";
import { columns } from "../Constants/QuotesTable";
import "./Quotes.css"
import { createImmediatelyInvokedFunctionExpression } from "typescript";

const Quotes = () => {
  const defaultValues = {
    totalQty: 0,
    combinations: 0,
  };

  const {
    handleSubmit,
    control,
    // formState: { errors },
    reset,
  } = useForm({ defaultValues });

  const [quotes, setQuotes] = useState([]);
  const [quoteToEdit, setQuoteToedit] = useState({});
  const [combinationsQty, setCombinations] = useState(0);
  const [quotesQty, setQuotesQty] = useState(0);
  const [pagesQty, setPagesQty] = useState(0);
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(false);
  const [modalCrud, setModal] = useState(false);
  const [tableHistory, setTableHistory] = useState({})

  const history = useHistory()

  useEffect(() => {
      history.listen((location) => {
          const id = location.pathname.split("/").pop()
          if(parseInt(id) || parseInt(id) === 0 ){
            QuotesApi.Get(id).then((res) => {
              setQuoteToedit(res);
              toggleModalCrud();
            });
          }
      })
  }, [history])


  const toggleModalCrud = () => {
    if(modalCrud == true) {
      if(quoteToEdit.id) history.goBack()
      setQuoteToedit({})
    }
    setModal(!modalCrud)
    
  }

  const getQuotes = (
    pageIndex = "0",
    pageSize = "5",
    sort = {},
    search = {}
  ) => {
    setTableHistory({
      pageIndex,
      pageSize
    })
    setLoading(true);
    QuotesApi.GetAll(
      pageIndex,
      pageSize,
    ).then((res) => {
      setQuotes(res.quotes);
      setQuotesQty(res.quotesQty);
      setPagesQty(res.pagesQty);
      setLoading(false);
    });
  };

  const onCalculate = (data) => {
    setLoading2(true)
    QuotesApi.GetCombinations(data.totalQty)
    .then((res) => {
      console.log("combinations", res)
      setCombinations(res);
      setLoading2(false)

    });
  };

  const searchAgain = () => {
    getQuotes(tableHistory.pageIndex, tableHistory.pageSize,)
  }

  return (
    <Fragment>
      <Form onSubmit={handleSubmit(onCalculate)}>
        <Row>
          <Col md={4}>
            <Label for="totalQty">Total length to calculate:</Label>
            <Controller
              control={control}
              name="totalQty"
              render={({ field }) => (
                <Input
                  {...field}
                />
              )}
            />
          </Col>
          <Col md={2} className="end-fix-container">
            <Button className="btn-primary">
              Buscar
            </Button>
          </Col>
          <Col md={1} className="end-fix-container">
            {
              loading2 ? (<Spinner color="primary">.</Spinner>) : (null)
            }
          </Col>
          <Col md={3} className="">
            <Label for="combinations">Combinations:</Label>
            <Input
            control={control}
            value={combinationsQty}
            name="combinations"
            disabled={true}>
            </Input>
          </Col>
          <Col md={2} className="end-fix-container">
            <Button color="danger" onClick={toggleModalCrud}>Add Quote</Button>
          </Col>
        </Row>
      </Form>
      

      <ModalCrud
      isOpen = {modalCrud}
      toggle = {toggleModalCrud}
      quoteToEdit = {quoteToEdit}
      searchAgain = {searchAgain}
      />

      <hr></hr>

      <CustomTable
        columns={columns}
        data={quotes}
        loading={loading}
        fetchData={getQuotes}
        pageCount={pagesQty}
        qtyRecords={quotesQty}
      />
      
      <hr></hr>
    </Fragment>
  );
};

export default Quotes;
