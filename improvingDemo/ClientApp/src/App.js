import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './Components/Layout/Layout';
import { Home } from './Pages/Home';
import Quotes from './Pages/Quotes';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/quotes' component={Quotes} />
      </Layout>
    );
  }
}
