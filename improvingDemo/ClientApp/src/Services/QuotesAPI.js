﻿import { urlConstants } from "../Constants/UrlConstants";
import { defaultHeader, HandleResponse } from "../Helpers/HttpUtils";

export const QuotesApi = {
  Get,
  GetAll,
  Save,
  Update,
  Delete,
  GetCombinations,
};

function Get(id) {
  const uri = "api/quotes/";
  const requestOptions = {
    method: "GET",
    headers: defaultHeader(),
  };

  const queryParams = id ? `${id}` : "";

  return fetch(`${urlConstants.URLAPI}${uri}${queryParams}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function GetAll(
  pagina,
  itemsPorPagina,
  intentos = 255
) {
  const uri = "api/quotes?";
  const requestOptions = {
    method: "GET",
    headers: defaultHeader(),
  };

  const queryParams =
    (pagina ? `page=${pagina}&` : "") +
    (itemsPorPagina ? `itemsPerPage=${itemsPorPagina}` : "") 

  return new Promise((resolve, reject) => {
    fetch(`${urlConstants.URLAPI}${uri}${queryParams}`, requestOptions)
      .then(HandleResponse)
      .then(resolve)
      .catch((error) => {
        console.log(error);
      });
  });
}

function Save(quote) {
  const uri = "api/quotes";
  
  const data = JSON.stringify(quote);
  const requestOptions = {
    method: "POST",
    headers: defaultHeader(),
    body: data,
  };

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function Update(quote) {
  const uri = "api/quotes";

  const data = JSON.stringify(quote);
  const requestOptions = {
    method: "PUT",
    headers: defaultHeader(),
    body: data,
  };

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function Delete(id) {
  const uri = "api/quotes/";

  const requestOptions = {
    method: "DELETE",
    headers: defaultHeader(),
  };

  const queryParams = id ? `${id}` : "";

  return fetch(`${urlConstants.URLAPI}${uri}${queryParams}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function GetCombinations(qty){
  const uri = "api/quotes/pairs/";

  const requestOptions = {
    method: "GET",
    headers: defaultHeader(),
  };

  const urlParams = qty ? `${qty}` : "0";

  return fetch(`${urlConstants.URLAPI}${uri}${urlParams}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function calculaEdad(cumpleaños) {
  var diff_ms = new Date().setHours(0, 0, 0, 0) - cumpleaños.getTime();
  var edad = new Date(diff_ms);

  return Math.abs(edad.getUTCFullYear() - 1970);
}
