﻿export {
    HandleResponse,
    defaultHeader
}

function HandleResponse(res) {
    if (!res.ok) throw res.status
    return res.json()
}

function defaultHeader() {
    return {
        "Accept": "application/json",
        "Content-Type": "application/json",
        // "mode": "no-cors",
    }
}