import React, {useEffect, useState} from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
  Label,
  Input
} from "reactstrap";
import { QuotesApi } from "../../Services/QuotesAPI";

const ModalCrud = ({ toggle, isOpen, quoteToEdit, searchAgain }) => {

  const [quote, setQuote] = useState({});
  const [nestedModal, setNestedModal] = useState(false);

  useEffect(() =>{
    if(quoteToEdit.id !== undefined){
      setQuote(quoteToEdit)
    }else{
      setQuote({})
    }
  },[isOpen])

  const toggleNested = () => {
    setNestedModal(!nestedModal);
  }

  const onValuesChange = (data) => {
    const fieldName = data.target.name
    const fieldValue = data.target.value
    setQuote({
      ...quote,
      [fieldName]: fieldValue,
    })
  }

  const onConfirm = () => {
    if(!quote.author || !quote.text){

    }else{
      if(quote.id >= 0){
        QuotesApi.Update(quote)
        .then((res) => {
          toggle()
          searchAgain()
        });
      }else{
        QuotesApi.Save(quote)
        .then((res) => {
          toggle()
          searchAgain()
        });
      }
    }
  }

  const onDelete = () => {
    QuotesApi.Delete(quote.id)
    .then((res) => {
      toggleNested()
      toggle()
      searchAgain()
    });
  }

  return (
    <div>
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>Quote</ModalHeader>
        <ModalBody>
          <Row>
            <Col md={12}>
              <Input
                hidden={true}
                value={quote.id}
              />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Label for="author">Author:</Label>
              <Input
                name="author"
                defaultValue={quote.author || ""}
                onChange={onValuesChange}
              />
            </Col>
          </Row>
          <Row>
            <Col md={12} className="">
              <Label for="text">Text:</Label>
              <Input
                type="textarea"
                name="text"
                defaultValue={quote.text || ""}
                onChange={onValuesChange}
              />
            </Col>
          </Row>
          <Modal isOpen={nestedModal} toggle={toggleNested}>
            <ModalHeader>Warning</ModalHeader>
            <ModalBody>Are you sure you want to delete this?</ModalBody>
            <ModalFooter>
              <Button color="danger" onClick={onDelete}>Yes</Button>{' '}
              <Button color="secondary" onClick={toggleNested}>No</Button>
            </ModalFooter>
          </Modal>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={onConfirm}>
            Save
          </Button>{" "}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
          {
            quote.id ? (
              <Button color="danger" onClick={toggleNested}>
              Delete
            </Button>
            ):(null)
          }
          
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalCrud;
