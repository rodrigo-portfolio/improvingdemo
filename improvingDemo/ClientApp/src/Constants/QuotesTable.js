import { Link } from "react-router-dom";
import editLogo from "../Resources/edit_24.svg";

export const columns = [
  {
    Header: "Quotes",   
    columns: [
      {
        Header: "",
        accessor: "id",
        Cell: (props) => {
          return (
            <Link to={`quotes/${props.value}`}>
              {" "}
              <img src={editLogo}></img>{" "}
            </Link>
          );
        },
        disableSortBy: true,
      },
      {
        Header: "Author",
        accessor: "author", 
        disableSortBy: true,
      },
      {
        Header: "Quote",
        accessor: "text",
        disableSortBy: true,
      },
    ],
  }
];
