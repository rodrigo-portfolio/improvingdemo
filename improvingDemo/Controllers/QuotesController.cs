﻿using improvingDemo.DB;
using improvingDemo.DB.Connection;
using improvingDemo.Models;
using Microsoft.AspNetCore.Mvc;

// I was going to add a custom controller class to handle exceptions but i saw you specify was not necessary, anyway I just wanted mention that i use to do it

namespace improvingDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {
        private QuotesDict quotesDict = QuotesDict.Instance;
        private IQuotesDB _quotesDB;

        public QuotesController(IQuotesDB quotesDB)
        {
            _quotesDB = quotesDB;
        }

        // GET: api/<QuotesController>
        [HttpGet]
        public ActionResult<QuotesPaged> Get([FromQuery] SearchParameters parameters)
        {
            return Ok(_quotesDB.GetAll(parameters));
        }

        // GET api/<QuotesController>/5
        [HttpGet("{id}")]
        public ActionResult<Quote> Get(int id)
        {
            var result = _quotesDB.GetById(id);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/<QuotesController>
        [HttpPost]
        public ActionResult<Quote> Post([FromBody] Quote quote)
        {
            var result = _quotesDB.Add(quote);
            return Created("quotes", result);
        }

        // PUT api/<QuotesController>/5
        [HttpPut]
        public ActionResult<Quote> Put([FromBody] Quote quote)
        {
            var result = _quotesDB.Edit(quote);
            if(result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
            
        }

        // DELETE api/<QuotesController>/5
        [HttpDelete("{id}")]
        public ActionResult<Quote> Delete(int id)
        {

            var result = _quotesDB.Delete(id);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("pairs/{length}")]
        public ActionResult<long> Pairs( int length)
        {
            var result = quotesDict.getUniquePairs(length);
            return Ok(result);
        }
    }
}
