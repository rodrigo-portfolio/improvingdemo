﻿namespace improvingDemo.Models
{
    public class SearchParameters
    {
        public int page { get; set; } = 0;
        public int itemsPerPage { get; set; } = 5;
    }
}
