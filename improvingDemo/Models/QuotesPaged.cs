﻿namespace improvingDemo.Models
{
    public class QuotesPaged
    {
        public QuotesPaged() { }

        public QuotesPaged(IEnumerable<Quote> pacientes, int _quotesQty, int itemsPerPage)
        {
            pagesQty = (int)Math.Ceiling((decimal)_quotesQty / itemsPerPage);
            quotesQty = _quotesQty;
            quotes = pacientes;
        }
        public int pagesQty { get; set; }
        public int quotesQty { get; set; }
        public IEnumerable<Quote> quotes { get; set; }
    }
}
