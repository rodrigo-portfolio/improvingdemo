﻿using improvingDemo.Models;
using System.Text.Json;

namespace improvingDemo.DB.Connection
{
    public class QuotesDB : IQuotesDB
    {
        private List<Quote> _quoteDB;
        string path; 
        private QuotesDict quotesDict = QuotesDict.Instance;
        public QuotesDB(IConfiguration config)
        {
            using (StreamReader r = new StreamReader(config.GetSection("settings").Value))
            {
                path = config.GetSection("settings").Value;
                string json = r.ReadToEnd();
                _quoteDB = JsonSerializer.Deserialize<List<Quote>>(json);
            }
        }
        public Quote Add(Quote quote)
        {
            quote.Id = _quoteDB.LastOrDefault() != null ? _quoteDB.LastOrDefault().Id + 1 : 1;
            _quoteDB.Add(quote);

            string json = JsonSerializer.Serialize(_quoteDB);

            File.WriteAllText(path, json);

            quotesDict.addItem(quote.Id, quote.Text.Length);
            return quote;
        }

        public Quote Delete(int id)
        {
            var quote = _quoteDB.FirstOrDefault(q => q.Id == id);

            if(quote != null)
            {
                _quoteDB.Remove(quote);

                string json = JsonSerializer.Serialize(_quoteDB);

                File.WriteAllText(path, json);

                quotesDict.removeItem(id);
            }
           
            return quote;
        }

        public Quote Edit(Quote quote)
        {
            var quotetoEdit = _quoteDB.FirstOrDefault(q => q.Id == quote.Id);

            if (quotetoEdit != null)
            {
                quotetoEdit.Author = quote.Author;
                quotetoEdit.Text = quote.Text;

                string json = JsonSerializer.Serialize(_quoteDB);

                File.WriteAllText(path, json);

                quotesDict.editItem(quotetoEdit.Id, quotetoEdit.Text.Length);
            }

            return quotetoEdit;
        }

        public QuotesPaged GetAll(SearchParameters parameters)
        {
            var quotesAndCount = SearchByParameters(parameters);


            return new QuotesPaged(quotesAndCount.Item1, quotesAndCount.Item2, parameters.itemsPerPage);
        }

        public Quote GetById(int id)
        {
            return _quoteDB.FirstOrDefault(q => q.Id == id);
        }

        private Tuple<IEnumerable<Quote>, int> SearchByParameters(SearchParameters parametros)
        {
            var query = _quoteDB.AsQueryable();       

            int rouwCount = query.Count();
            return new Tuple<IEnumerable<Quote>, int>
                (query.Skip(parametros.page * parametros.itemsPerPage).Take(parametros.itemsPerPage),
                rouwCount);
        }

    }
}
