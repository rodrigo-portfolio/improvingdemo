﻿using improvingDemo.Models;

namespace improvingDemo.DB.Connection
{
    public interface IQuotesDB
    {
        public QuotesPaged GetAll(SearchParameters parameters);
        public Quote GetById(int id);
        public Quote Add(Quote quote);
        public Quote Edit(Quote quote);
        public Quote Delete(int id);
    }
}
