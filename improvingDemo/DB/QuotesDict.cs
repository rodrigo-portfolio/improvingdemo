﻿using improvingDemo.Models;
using System.Text.Json;

namespace improvingDemo.DB
{
    public class QuotesDict
    {
        private Dictionary<int, int> quotes = new Dictionary<int, int>();
       
        IConfiguration configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile($"appsettings.json").Build();


        private QuotesDict()
        {
            using (StreamReader r = new StreamReader(configuration.GetSection("settings").Value))
            {
                Console.WriteLine("Reading File and saving data into Dictionary");
                string json = r.ReadToEnd();
                IEnumerable<Quote> quoteItems = JsonSerializer.Deserialize<List<Quote>>(json);
                foreach (var quoteItem in quoteItems)
                {
                    quotes.Add(quoteItem.Id, quoteItem.Text.Length);
                }
                Console.WriteLine("Dictionary ready");
            }
        }

        private static QuotesDict instance = new QuotesDict();
        public static QuotesDict Instance => instance;        

        public long getUniquePairs (int charsQty)
        {
            var indexesPairs = new List<Tuple<int, int>>();
            long count = 0;
            var temporaryDict = new Dictionary<int, int>(quotes.Where(i => i.Value < charsQty).OrderBy(i => i.Value));
            foreach (var quote in temporaryDict)
            {

                if (quote.Value > charsQty / 2)
                {
                    break;
                }
                int complementNumber= charsQty - quote.Value;
            
                count += temporaryDict.Where(k => k.Value <= complementNumber
                    && k.Key != quote.Key).Count();

                temporaryDict.Remove(quote.Key);
            }
            return count;
        }

        public void setQuotes(IEnumerable<Quote> quotesList)
        {
            foreach(var quoteItem in quotesList)
            {
                quotes.Add(quoteItem.Id, quoteItem.Text.Length);
            }
        }

        public void removeItem(int index)
        {
            quotes.Remove(index);
        }
        public void addItem(int index, int length)
        {
            quotes.Add(index, length);
        }

        public void editItem(int index, int length)
        {
            quotes[index] = length;
        }
    }

}

